
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

@pytest.fixture
def setup_data():
    print("\nSetting up resources...")
    driver = webdriver.Remote(
        command_executor='http://selenium__standalone-chrome:4444/wd/hub',
        options=Options()
    )
    driver.get('https://the-internet.herokuapp.com/context_menu')
    driver.maximize_window()
    yield driver  
    print("\nTearing down resources...")
    driver.quit()
 
def test_string_is_found_on_the_page(setup_data):
    driver = setup_data
    test_string = "Right-click in the box below to see one called \'the-internet\'"
    
    try:
        content = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, "content"))
        )
        assert test_string in content.text

    except TimeoutException:
        print("Loading took too much time!")
        assert False
    
def test_should_fail_if_string_is_not_on_the_page(setup_data):
    driver = setup_data
    test_string = "Alibaba"
    
    try:
        content = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, "content"))
        )
        assert test_string in content.text

    except TimeoutException:
        print("Loading took too much time!")
        assert False

def test_correct_page_title_is_shown(setup_data):
    driver = setup_data
    title = "The Internet"
    assert title == driver.title

def test_show_alert_message_on_box_context_click(setup_data):
    driver = setup_data
    message = "You selected a context menu"
    try:
        context_box = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, "hot-spot"))
        )

        ActionChains(driver).context_click(context_box).perform()
        alert = WebDriverWait(driver, 5).until(
            EC.alert_is_present()
        )
        assert alert.text == message

    except TimeoutException:
        print("Loading took too much time!")
        assert False

  

    
  
   
    